from django.conf.urls import url
from .views import user_register, logout_view, login_view

urlpatterns = [
    url(r'^register/', user_register, name='register'),
    url(r'^login/', login_view, name='login'),
    url(r'^logout/', logout_view, name='logout'),

]