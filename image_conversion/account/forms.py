from django import forms
from .models import Person

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

User = get_user_model()


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(label='Email Address')

    class Meta:
        model = Person
        fields = [
            'email',
        ]

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        email_qs = User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError('This email has already  been registered')
        return super(UserRegisterForm, self).clean(*args, **kwargs)


class UserLoginForm(forms.Form):
    email = forms.EmailField(label='Email Address')
    registration_id = forms.CharField()

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get("email")
        reg_id = self.cleaned_data.get("registration_id")
        user = authenticate(email=email, registration_id=reg_id)
        if not user:
            raise forms.ValidationError("this user does not exist")