# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import messages

from django.shortcuts import render, redirect
from .forms import UserRegisterForm, UserLoginForm
from django.contrib.auth import (
    authenticate,
    login,
    logout
)
from datetime import datetime


# Create your views here.
# To generate register id
def generate_register_id(user):
    today = datetime.today()
    register_id = str(user.id) + str(today.month) + str(today.year)
    user.registration_id = register_id
    user.save()


# user Register view
def user_register(request):
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        email = form.cleaned_data.get("email")
        user.username = email
        user.save()
        generate_register_id(user)
        new_user = authenticate(request, email=user.email, registration_id=user.registration_id)
        login(request, new_user)
        messages.success(request, 'You are Successfully LoggedIn to Application')
    return render(request, 'register.html', {'form': form})


# Login View
def login_view(request):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data.get("email")
        registration_id = form.cleaned_data.get("registration_id")
        user = authenticate(request, email=email, registration_id=registration_id)
        login(request, user)
        messages.success(request, 'You are Successfully LoggedIn to Application')
    return render(request, 'login.html', {'form': form})


# Logout View
def logout_view(request):
    logout(request)
    return redirect('/')
