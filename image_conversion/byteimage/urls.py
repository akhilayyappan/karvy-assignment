from django.conf.urls import url
from .views import home, image_to_byte, upload_image

urlpatterns = [
    url(r'^home/', home, name='home'),
    url(r'^upload-image/', upload_image, name='image-upload'),
    url(r'^image-to-byte/(?P<id>\d+)/$', image_to_byte, name="image-conversion"),
]