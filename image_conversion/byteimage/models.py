# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.urlresolvers import reverse

from .validators import validate_file_extension
# Create your models here.

class ImageUpload(models.Model):
    name = models.CharField(max_length=20)
    image = models.ImageField(upload_to='images', blank=False, null=True, validators=[validate_file_extension])

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('image-conversion', args=(self.id,))
