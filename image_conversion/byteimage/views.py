# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import ImageUploadForm
from .models import ImageUpload
import base64
from image_conversion.settings import BASE_DIR
from django.contrib import messages


# Create your views here.
def home(request):
    return render(request, 'home.html', {})


# Image Upload View
def upload_image(request):
    if request.user.is_authenticated():
        form = ImageUploadForm(request.POST, request.FILES)
        if form.is_valid():
            image_byte = form.save(commit=False)
            image_byte.save()
            return HttpResponseRedirect(image_byte.get_absolute_url())
        context = {
            'form': form
        }
    else:
        messages.warning(request, 'You are not authorised to access this page')
        context = {}
    return render(request, 'image_upload_form.html', context)


# Byte Convesion View
def image_to_byte(request, id):
    obj = ImageUpload.objects.get(id=id)
    image_url = obj.image.url
    image_path = BASE_DIR + image_url
    with open(image_path, "rb") as imageFile:
        str = base64.b64encode(imageFile.read())
        length_of_string = len(str)
    context = {
        'obj': obj,
        'length_of_string': length_of_string,
        'string': str
    }
    return render(request, 'image_upload_form.html', context)
